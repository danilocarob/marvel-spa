import { Component, Output, EventEmitter } from '@angular/core';
import { MarvelApiService } from './Service/marvel-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Marvel-spa';
  @Output('newComic') newComic = new EventEmitter();
  favoriteList: any = [];
  listIds: any = [];
  heros: any = [];

  constructor(private _marvelService: MarvelApiService) { }

  ngOnInit(): void {
    this.favoriteList = JSON.parse(localStorage.getItem('favoriteList') || '[]');
  }

  addComic(comic: any) {
    this.listIds = this.favoriteList.map(function (comic: any = []) {
      return comic.id
    });

    const comicExist = this.listIds.some((e: any) => e === comic.id);

    if (!comicExist) {
      this.favoriteList.push(comic);
      localStorage.setItem(
        'favoriteList',
        JSON.stringify(this.favoriteList)
      );
    }
  }

  removeComic(comicRemove: any) {
    this.favoriteList = this.favoriteList.filter((e: any) => e.id !== comicRemove.id);
    localStorage.setItem(
      'favoriteList',
      JSON.stringify(this.favoriteList)
    );
  }

  searchHeros(searchHero: any) {
    console.log('busqueda', searchHero);
    this.heros = searchHero;
  }

  clickRandom() {
    this._marvelService.getRamdomComic().subscribe((value) => {
      let comics: any[] = value.data.results;
      console.log('estos comic', comics[0]);
      
      comics = comics.sort(() => { return (Math.random() - 0.5) });

      let cont = 0;
      comics.forEach(comic => {
        const comicExist = this.listIds.some((e: any) => e === comic.id);
        if (!comicExist && cont < 3) {
          this.favoriteList.push(comic);
          localStorage.setItem(
            'favoriteList',
            JSON.stringify(this.favoriteList)
          );
          cont++;
        }
      });

    })
  }
}
