import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MarvelApiService {
  private baseMarvelAPI;
  private apiKey;
  private hash;

  constructor(private _http: HttpClient) {
    this.baseMarvelAPI = environment.baseMarvelAPI;
    this.apiKey = environment.apiKey;
    this.hash = environment.hash;
  }

  public getHerosByPage(page: number = 0, number: number = 10): Observable<any> {
    page = page * number;
    return this._http.get(this.baseMarvelAPI + 'characters?&offset=' + page + '&limit=' + number + '&ts=1&apikey=' + this.apiKey + '&hash=' + this.hash);
  }

  public getComics(heroId: any): Observable<any> {
    return this._http.get(this.baseMarvelAPI + 'characters/' + heroId + '/comics?limit=100&ts=1&apikey=' + this.apiKey + '&hash=' + this.hash);
  }

  getRamdomComic(): Observable<any> {
    return this._http.get(this.baseMarvelAPI + 'comics?limit=100&ts=1&apikey=' + this.apiKey + '&hash=' + this.hash);
  }

  searchHero(nameStartsWith: any): Observable<any> {
    return this._http.get(this.baseMarvelAPI + 'characters?nameStartsWith=' + nameStartsWith + '&limit=100&ts=1&apikey=' + this.apiKey + '&hash=' + this.hash);
  }

}