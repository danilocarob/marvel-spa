import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {
  public pages = [0, 1, 2, 3];
  public actualPage = 0;
  @Output('newPage') newPage = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.changePage(this.actualPage);
  }

  changePage(page: any) {
    this.newPage.emit(page);
    this.actualPage = page;
  }

}
