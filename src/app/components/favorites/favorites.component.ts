import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {
  @Input('favoriteComic') favoriteComic: any;
  @Output('removeComic') removeComic = new EventEmitter();
  @Output('clickRandom') clickRandom = new EventEmitter();


  constructor() { }

  ngOnInit(): void {
  }

  removeFavorites(comic: any) {
    this.removeComic.emit(comic);
  }

  RandomFavorite() {
    this.clickRandom.emit();
  }

}
