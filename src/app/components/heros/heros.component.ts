import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MarvelApiService } from 'src/app/Service/marvel-api.service';


@Component({
  selector: 'app-heros',
  templateUrl: './heros.component.html',
  styleUrls: ['./heros.component.css']
})
export class HerosComponent implements OnInit {
  @Input('actualPage') actualPage: any;
  @Input('heros') heros = [];
  public number: number = 10;
  public page = 1;
  public loading = false;
  @Output('newComic') newComic = new EventEmitter();

  constructor(private _marvelService: MarvelApiService) { }

  ngOnInit(): void {
    this.loading = true;
    this.loadHeros();
  }

  loadHeros() {
    this._marvelService.getHerosByPage(this.actualPage, this.number).subscribe((value) => {
      this.heros = value.data.results;
      this.loading = false;
    })
  }

  changePage(page: any) {
    this.actualPage = page;
    this.loadHeros();
  }

  addComic(comic: any) {
    this.newComic.emit(comic);
  }

}
