import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MarvelApiService } from 'src/app/Service/marvel-api.service';

@Component({
  selector: 'app-comics',
  templateUrl: './comics.component.html',
  styleUrls: ['./comics.component.css']
})
export class ComicsComponent implements OnInit {
  @Input('heroId') heroId: any;
  @Output('newComic') newComic = new EventEmitter();
  public comics: any = [];
  favoriteList: any = [];
  public loading = false;
  public clicked = false;

  constructor(private _marvelService: MarvelApiService) { }

  ngOnInit(): void {
    this.loading = true;
    this._marvelService.getComics(this.heroId).subscribe((value) => {
      this.comics = value.data.results;
      this.loading = false;
    });
  }

  comicTouch(comic: any) {
    this.newComic.emit(comic);
  }


}
