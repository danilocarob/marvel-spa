import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MarvelApiService } from 'src/app/Service/marvel-api.service';
import { debounceTime } from "rxjs/operators";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Output('searchHeros') searchHeros = new EventEmitter();
  search = new FormControl('');
  constructor(private _marvelService: MarvelApiService) { }

  ngOnInit(): void {
    this.search.valueChanges.pipe(debounceTime(200)).subscribe((value) => {
      this.onSearchHero(value);
    });
  }

  onSearchHero(search: string) {
    console.log(search);
    let request = search ? this._marvelService.searchHero(search) : this._marvelService.getHerosByPage()
    request.subscribe((value) => {
      this.searchHeros.emit(value.data.results);
    })
  }

}
