import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})
export class HeroComponent implements OnInit {
  @Input('hero') hero: any = {};
  @Output('newComic') newComic = new EventEmitter();

  public heroId = 0;

  constructor() { }

  ngOnInit(): void {
  }

  openDetail(id: any) {
    this.heroId = id;
  }

  changePage(comic: any) {
    this.newComic.emit(comic);
  }
}
